# Monolith as a resource server demo app

## Prerequisite
- Java 17
- PostgreSQL 14.6
- Maven 3x

## Tech stack
- Spring boot 2.7.5 
- JPA
- Flyway
- Lombok 1.18.24
- Mapstruct 1.5.3.Final

## Run on Development mode

- Configure proper datasource in Environment Variables below:
  MONO_DB_DOMAIN=localhost;MONO_DB_NAME=monoappdb;MONO_DB_PASSWORD=cong1979;MONO_DB_PORT=5432;MONO_DB_SCHEMA=monoapp;MONO_DB_USERNAME=monodbadmin
- Run on IDE

## Build

- Maven build: mvn clean install

