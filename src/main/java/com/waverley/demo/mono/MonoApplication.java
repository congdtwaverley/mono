package com.waverley.demo.mono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

@SpringBootApplication()
public class MonoApplication {
	public static void main(String[] args) {
		SpringApplication.run(MonoApplication.class, args);
	}

}
