package com.waverley.demo.mono.constant;

public class AuthConstant {
    public static final String MONO_ADMIN_ROLE = "mono_admin";
    public static final String MONO_USER_ROLE = "mono_user";

}
