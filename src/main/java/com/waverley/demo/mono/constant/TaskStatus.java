package com.waverley.demo.mono.constant;

import lombok.Getter;

@Getter
public enum TaskStatus {
    CREATED("CREATED"),
    IN_PROGRESS("IN_PROGRESS");
    private String status;
    TaskStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
