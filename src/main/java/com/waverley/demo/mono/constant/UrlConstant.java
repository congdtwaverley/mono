package com.waverley.demo.mono.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UrlConstant {
    public static final String API = "/api";
    public static final String API_V1_VERSION = "/v1";
    public static final String USER_TASK_URL = "/task";

    public static final String USER_URL = "/user";
}
