package com.waverley.demo.mono.constant;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum UserStatus {
    CREATED("CREATED"),
    UPDATED("UPDATED");
    private String status;

    UserStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
