package com.waverley.demo.mono.controller;

import com.waverley.demo.mono.constant.AuthConstant;
import com.waverley.demo.mono.constant.UrlConstant;
import com.waverley.demo.mono.dto.BaseResponseDTO;
import com.waverley.demo.mono.dto.MonoUserRequestDTO;
import com.waverley.demo.mono.dto.MonoUserResponseDTO;
import com.waverley.demo.mono.dto.UserTaskResponseDTO;
import com.waverley.demo.mono.service.MonoUserService;
import com.waverley.demo.mono.service.UserTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(UrlConstant.API + UrlConstant.API_V1_VERSION + UrlConstant.USER_URL)
public class UserController {

    @Autowired
    private MonoUserService monoUserService;

    @Autowired
    private UserTaskService userTaskService;

    @PostMapping
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponseDTO<MonoUserResponseDTO> create(@RequestBody MonoUserRequestDTO requestDTO) {
        BaseResponseDTO<MonoUserResponseDTO> response = new BaseResponseDTO<>();
        response.setData(monoUserService.create(requestDTO));
        return response;
    }


    @GetMapping("/{id}")
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    @ResponseStatus(HttpStatus.FOUND)
    public BaseResponseDTO<MonoUserResponseDTO> getUserById(@PathVariable("id") UUID id) {
        BaseResponseDTO<MonoUserResponseDTO> response = new BaseResponseDTO<>();
        response.setData(monoUserService.getUserById(id));
        return response;
    }

    @GetMapping
    @RolesAllowed(AuthConstant.MONO_ADMIN_ROLE)
    public BaseResponseDTO<List<MonoUserResponseDTO>> getAllUsers() {
        BaseResponseDTO<List<MonoUserResponseDTO>> response = new BaseResponseDTO<>();
        response.setData(monoUserService.getAllUsers());
        return response;
    }


    @GetMapping("/search")
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    public BaseResponseDTO<MonoUserResponseDTO> findByCriteria(@RequestParam String userName) {
        BaseResponseDTO<MonoUserResponseDTO> response = new BaseResponseDTO<>();
        response.setData(monoUserService.getByUserName(userName));
        return response;
    }

    @PutMapping()
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    @ResponseStatus(HttpStatus.OK)
    public BaseResponseDTO<MonoUserResponseDTO> update(@RequestBody MonoUserRequestDTO requestDTO) {
        BaseResponseDTO<MonoUserResponseDTO> response = new BaseResponseDTO<>();
        response.setData(monoUserService.update(requestDTO));
        return response;
    }


    @GetMapping("/{userName}/task")
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    public BaseResponseDTO<List<UserTaskResponseDTO>> getUserTasks(@PathVariable("userName") String userName, @RequestParam(value = "status", required = false) List<String> status) {
        BaseResponseDTO<List<UserTaskResponseDTO>> response = new BaseResponseDTO<>();
        response.setData(userTaskService.getTasksByUserNameAndStatus(userName, status));
        return response;
    }
}
