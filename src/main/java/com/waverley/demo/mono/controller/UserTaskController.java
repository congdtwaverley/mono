package com.waverley.demo.mono.controller;

import com.waverley.demo.mono.constant.AuthConstant;
import com.waverley.demo.mono.constant.UrlConstant;
import com.waverley.demo.mono.dto.BaseResponseDTO;
import com.waverley.demo.mono.dto.UserTaskRequestDTO;
import com.waverley.demo.mono.dto.UserTaskResponseDTO;
import com.waverley.demo.mono.service.UserTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(UrlConstant.API + UrlConstant.API_V1_VERSION + UrlConstant.USER_TASK_URL)
public class UserTaskController {
    @Autowired
    private UserTaskService userTaskService;

    @PostMapping
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    @ResponseStatus(HttpStatus.OK)
    public BaseResponseDTO<UserTaskResponseDTO> create(@RequestBody UserTaskRequestDTO requestDTO) {
        BaseResponseDTO<UserTaskResponseDTO> response = new BaseResponseDTO<>();
        response.setData(userTaskService.create(requestDTO));
        return response;
    }

    @GetMapping("/{id}")
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    @ResponseStatus(HttpStatus.FOUND)
    public BaseResponseDTO<UserTaskResponseDTO> getTask(@PathVariable("id") UUID id) {
        BaseResponseDTO<UserTaskResponseDTO> response = new BaseResponseDTO<>();
        response.setData(userTaskService.getTaskById(id));
        return response;
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE,AuthConstant.MONO_USER_ROLE})
    public BaseResponseDTO<UserTaskResponseDTO> update(@RequestBody UserTaskRequestDTO requestDTO) {
        BaseResponseDTO<UserTaskResponseDTO> response = new BaseResponseDTO<>();
        response.setData(userTaskService.update(requestDTO));
        return response;
    }


    @DeleteMapping("/{id}")
    @RolesAllowed({AuthConstant.MONO_ADMIN_ROLE})
    public BaseResponseDTO<String> delete(@PathVariable("id") UUID id) {
        BaseResponseDTO<String> response = new BaseResponseDTO<>();
        response.setData(userTaskService.deleteTaskById(id));
        return response;
    }
}
