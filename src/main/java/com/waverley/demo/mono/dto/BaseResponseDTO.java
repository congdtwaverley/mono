package com.waverley.demo.mono.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class BaseResponseDTO<T> implements Serializable {
    private T data;
}
