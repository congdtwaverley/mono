package com.waverley.demo.mono.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonoUserResponseDTO implements Serializable {
    private UUID id;
    private String authId;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String status;
}
