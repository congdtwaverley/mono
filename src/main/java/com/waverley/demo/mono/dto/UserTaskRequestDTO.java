package com.waverley.demo.mono.dto;

import com.waverley.demo.mono.entity.MonoUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTaskRequestDTO implements Serializable {
    private UUID id;
    private MonoUserRequestDTO monoUser;
    private String title;
    private String description;
    private String status;
}
