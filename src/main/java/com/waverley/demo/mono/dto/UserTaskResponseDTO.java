package com.waverley.demo.mono.dto;

import com.waverley.demo.mono.entity.MonoUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTaskResponseDTO {
    private UUID id;
    private MonoUserResponseDTO monoUser;
    private String title;
    private String description;
    private String status;
}
