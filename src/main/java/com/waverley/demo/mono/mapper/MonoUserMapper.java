package com.waverley.demo.mono.mapper;

import com.waverley.demo.mono.dto.MonoUserResponseDTO;
import com.waverley.demo.mono.entity.MonoUser;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring",
//        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Component
@Slf4j
public abstract class MonoUserMapper {
    public static final MonoUserMapper INSTANCE = Mappers.getMapper(MonoUserMapper.class);

    @Mapping(target = "id", source = "monoUser.id")
    @Mapping(target = "authId", source = "monoUser.authId")
    @Mapping(target = "userName", source = "monoUser.userName")
    @Mapping(target = "firstName", source = "monoUser.firstName")
    @Mapping(target = "lastName", source = "monoUser.lastName")
    @Mapping(target = "email", source = "monoUser.email")
    @Mapping(target = "status", source = "monoUser.status")
    public abstract MonoUserResponseDTO mapEntityToResponseDTO(MonoUser monoUser);
}
