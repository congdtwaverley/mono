package com.waverley.demo.mono.mapper;

import com.waverley.demo.mono.dto.MonoUserResponseDTO;
import com.waverley.demo.mono.dto.UserTaskResponseDTO;
import com.waverley.demo.mono.entity.UserTask;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Mapper(componentModel = "spring",
//        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
@Component
@Slf4j
public abstract class UserTaskMapper {
    public static final UserTaskMapper INSTANCE = Mappers.getMapper(UserTaskMapper.class);
    @Mapping(target = "id", source = "task.id")
    @Mapping(target = "monoUser", source = "userDTO")
    @Mapping(target = "title", source = "task.title")
    @Mapping(target = "description", source = "task.description")
    @Mapping(target = "status", source = "task.status")
    public abstract UserTaskResponseDTO mapEntityToResponseDTO(UserTask task, MonoUserResponseDTO userDTO);
}
