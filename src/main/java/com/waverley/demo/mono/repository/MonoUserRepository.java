package com.waverley.demo.mono.repository;

import com.waverley.demo.mono.entity.MonoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MonoUserRepository extends JpaRepository<MonoUser, UUID> {
    Optional<MonoUser> findFirstByUserName(String userName);
}
