package com.waverley.demo.mono.repository;

import com.waverley.demo.mono.entity.MonoUser;
import com.waverley.demo.mono.entity.UserTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserTaskRepository extends JpaRepository<UserTask, UUID> {
    @Query("SELECT ut FROM UserTask ut JOIN MonoUser mu ON ut.monoUser = mu WHERE mu.userName = :userName")
    List<UserTask> findAllTaskByUserName(@Param("userName") String userName);

//    @Modifying
//    @Query("UPDATE UserTask t SET t.title = :title, t.description = :description, t.status = :status WHERE t.id = :id")
//    UserTask updateTask(@Param("title") String title, @Param("description") String description, @Param("status") String status, @Param("id") UUID id);
}
