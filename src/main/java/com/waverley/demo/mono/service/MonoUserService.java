package com.waverley.demo.mono.service;

import com.waverley.demo.mono.constant.UserStatus;
import com.waverley.demo.mono.dto.MonoUserRequestDTO;
import com.waverley.demo.mono.dto.MonoUserResponseDTO;
import com.waverley.demo.mono.entity.MonoUser;
import com.waverley.demo.mono.mapper.MonoUserMapper;
import com.waverley.demo.mono.repository.MonoUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class MonoUserService {
    @Autowired
    private MonoUserRepository monoUserRepository;

    @Transactional
    public MonoUserResponseDTO create(MonoUserRequestDTO requestDTO) {
        MonoUser user = monoUserRepository.findFirstByUserName(requestDTO.getUserName()).orElse(null);
        if (user != null) {
            log.error("User with useName: [{}] is existing already", requestDTO.getUserName());
            throw new IllegalArgumentException("User is existing");
        }
        user = new MonoUser();
        user.setAuthId(requestDTO.getAuthId());
        user.setUserName(requestDTO.getUserName());
        user.setEmail(requestDTO.getEmail());
        user.setFirstName(requestDTO.getFirstName());
        user.setLastName(requestDTO.getLastName());
        user.setStatus(UserStatus.CREATED.getStatus());
        user = monoUserRepository.saveAndFlush(user);

        MonoUserResponseDTO responseDTO = MonoUserMapper.INSTANCE.mapEntityToResponseDTO(user);
        return responseDTO;
    }

    @Transactional
    public MonoUserResponseDTO update(MonoUserRequestDTO requestDTO) {
        MonoUser user = monoUserRepository.findFirstByUserName(requestDTO.getUserName()).orElse(null);
        if (user == null) {
            log.error("User with useName: [{}] doesn't exist", requestDTO.getUserName());
            throw new IllegalArgumentException("User doesn't exist");
        }

        user.setAuthId(requestDTO.getAuthId());
        user.setEmail(requestDTO.getEmail());
        user.setFirstName(requestDTO.getFirstName());
        user.setLastName(requestDTO.getLastName());
        user.setStatus(UserStatus.UPDATED.getStatus());
        user = monoUserRepository.saveAndFlush(user);

        MonoUserResponseDTO responseDTO = MonoUserMapper.INSTANCE.mapEntityToResponseDTO(user);
        return responseDTO;
    }

    @Transactional
    public MonoUserResponseDTO getUserById(UUID id) {
        MonoUser user = monoUserRepository.findById(id).orElse(null);
        if (user == null) {
            log.error("User with id: [{}] not found", id.toString());
        }
        return MonoUserMapper.INSTANCE.mapEntityToResponseDTO(user);
    }

    @Transactional
    public MonoUserResponseDTO getByUserName(String userName) {
        MonoUser user = monoUserRepository.findFirstByUserName(userName).orElse(null);
        if (user == null) {
            log.error("User with userName: [{}] not found", userName);
            return null;
        }
        return MonoUserMapper.INSTANCE.mapEntityToResponseDTO(user);
    }

    @Transactional
    public List<MonoUserResponseDTO> getAllUsers() {
        List<MonoUser> users = monoUserRepository.findAll();
        List<MonoUserResponseDTO> result = new ArrayList<>();
        users.forEach(u -> {
            result.add(MonoUserMapper.INSTANCE.mapEntityToResponseDTO(u));
        });
        return result;
    }
}
