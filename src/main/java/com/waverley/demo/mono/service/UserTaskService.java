package com.waverley.demo.mono.service;

import com.waverley.demo.mono.constant.TaskStatus;
import com.waverley.demo.mono.dto.MonoUserResponseDTO;
import com.waverley.demo.mono.dto.UserTaskRequestDTO;
import com.waverley.demo.mono.dto.UserTaskResponseDTO;
import com.waverley.demo.mono.entity.MonoUser;
import com.waverley.demo.mono.entity.UserTask;
import com.waverley.demo.mono.mapper.MonoUserMapper;
import com.waverley.demo.mono.mapper.UserTaskMapper;
import com.waverley.demo.mono.repository.MonoUserRepository;
import com.waverley.demo.mono.repository.UserTaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Slf4j
@Service
public class UserTaskService {
    @Autowired
    private UserTaskRepository userTaskRepository;

    @Autowired
    private MonoUserRepository monoUserRepository;

    @Transactional
    public UserTaskResponseDTO create(UserTaskRequestDTO requestDTO) {
        MonoUser user = monoUserRepository.findFirstByUserName(requestDTO.getMonoUser().getUserName()).orElse(null);
        if (user == null) {
            log.error("User [{}] not found", requestDTO.getMonoUser().getUserName());
            throw new IllegalArgumentException("User not found");
        }
        UserTask task = new UserTask();
        task.setMonoUser(user);
        task.setTitle(requestDTO.getTitle());
        task.setDescription(requestDTO.getDescription());
        task.setStatus(TaskStatus.CREATED.getStatus());
        task = userTaskRepository.saveAndFlush(task);

        MonoUserResponseDTO userResponseDTO = MonoUserMapper.INSTANCE.mapEntityToResponseDTO(user);
        UserTaskResponseDTO responseDTO = UserTaskMapper.INSTANCE.mapEntityToResponseDTO(task,userResponseDTO);

        return responseDTO;
    }

    @Transactional
    public UserTaskResponseDTO update(UserTaskRequestDTO requestDTO) {

        UserTask task = userTaskRepository.findById(requestDTO.getId()).orElse(null);

        if (task == null) {
            log.error("Task with id: [{}] not exist", requestDTO.getId().toString());
            throw new IllegalArgumentException("Task not exist");
        }

        task.setTitle(requestDTO.getTitle());
        task.setDescription(requestDTO.getDescription());
        task.setStatus(requestDTO.getStatus());

        task = userTaskRepository.saveAndFlush(task);

        MonoUserResponseDTO userResponseDTO = MonoUserMapper.INSTANCE.mapEntityToResponseDTO(task.getMonoUser());
        UserTaskResponseDTO responseDTO = UserTaskMapper.INSTANCE.mapEntityToResponseDTO(task,userResponseDTO);

        return responseDTO;
    }

    public UserTaskResponseDTO getTaskById(UUID id) {
        UserTask task = userTaskRepository.findById(id).orElse(null);
        if (task == null) {
            throw new NoResultException("The task not found");
        }

        UserTaskResponseDTO responseDTO = UserTaskMapper.INSTANCE.mapEntityToResponseDTO(task, MonoUserMapper.INSTANCE.mapEntityToResponseDTO(task.getMonoUser()));

        return responseDTO;
    }

    public String deleteTaskById(UUID id) {
        userTaskRepository.deleteById(id);
        return "DELETED";
    }

    public List<UserTaskResponseDTO> getTasksByUserNameAndStatus(String userName, List<String> status) {
        List<UserTask> tasks = userTaskRepository.findAllTaskByUserName(userName);
        List<UserTaskResponseDTO> responseDTOs = new ArrayList<>();
        for (UserTask task : tasks) {
            UserTaskResponseDTO responseDTO = UserTaskMapper.INSTANCE.mapEntityToResponseDTO(task, MonoUserMapper.INSTANCE.mapEntityToResponseDTO(task.getMonoUser()));
            responseDTOs.add(responseDTO);
        }
        return responseDTOs;
    }
}
