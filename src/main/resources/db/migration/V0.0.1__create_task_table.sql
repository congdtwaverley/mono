
CREATE TABLE IF NOT EXISTS monoapp.mono_user (
    id uuid PRIMARY KEY  NOT NULL,
    auth_id CHARACTER VARYING(50),
    user_name CHARACTER VARYING(255),
    first_name CHARACTER VARYING(255),
    last_name CHARACTER VARYING(255),
    email CHARACTER VARYING(255),
    status CHARACTER VARYING(50)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE monoapp.mono_user
    OWNER to monodbadmin;


CREATE TABLE IF NOT EXISTS monoapp.user_task (
    id uuid PRIMARY KEY NOT NULL,
    user_id uuid,
    title CHARACTER VARYING(255),
    description CHARACTER VARYING(500),
    status CHARACTER VARYING(50),
    CONSTRAINT user_task_mono_user_fk FOREIGN KEY (user_id) REFERENCES monoapp.mono_user (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE monoapp.user_task
    OWNER to monodbadmin;